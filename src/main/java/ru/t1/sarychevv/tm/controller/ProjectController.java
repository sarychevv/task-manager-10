package ru.t1.sarychevv.tm.controller;

import ru.t1.sarychevv.tm.api.IProjectController;
import ru.t1.sarychevv.tm.api.IProjectService;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.out.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        int index = 0;
        final List<Project> projects = projectService.findAll();
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getId() + ": " + project.getName() + " " + project.getDescription());
            index++;
        }
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECTS CLEAR]");
        projectService.clear();
        System.out.println("[OK]");
    }
}
