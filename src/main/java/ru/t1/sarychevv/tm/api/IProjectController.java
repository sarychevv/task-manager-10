package ru.t1.sarychevv.tm.api;

public interface IProjectController {

    void createProject();

    void showProjects();

    void clearProjects();

}
