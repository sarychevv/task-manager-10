package ru.t1.sarychevv.tm.api;

import ru.t1.sarychevv.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
