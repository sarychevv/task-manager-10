package ru.t1.sarychevv.tm.api;

public interface ITaskController {

    void createTask();

    void showTasks();

    void clearTasks();

}
