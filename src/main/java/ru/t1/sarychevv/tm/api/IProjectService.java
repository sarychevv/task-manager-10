package ru.t1.sarychevv.tm.api;

import ru.t1.sarychevv.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
